import TileMap from "./TileMap";

/**
 * Represents a type of tile.
 */
export enum TileType {
  Grass,
  Leaves,
  Flowers,
  Stalks,
  Tree,
  Mushrooms,
  Dirt,
  Fence,
}

/**
 * Represents a type of entity.
 */
export enum EntityType {
  Ghost,
  Human,
  Gravestone,
}

/**
 * Represents an entity.
 */
export type Entity =
  | { type: EntityType.Ghost; id: number; steps: number; hasMoved: boolean }
  | { type: EntityType.Human }
  | { type: EntityType.Gravestone };

/**
 * Represents a simulation grid.
 */
class Grid {
  static TILE_MAPS = [
    { src: "/img/01.png", tileSize: 16 },
    { src: "/img/02.png", tileSize: 16 },
    { src: "/img/03.png", tileSize: 16 },
  ];
  static TILE_SIZE = 64;

  tileMaps: TileMap[];

  grid: Map<string, TileType>;
  simulationSize: number;

  constructor(simulationSize: number) {
    this.tileMaps = [];

    this.grid = new Map();
    this.simulationSize = simulationSize;
  }

  /**
   * Load the tilemaps.
   */
  async init() {
    this.tileMaps = await Promise.all(
      Grid.TILE_MAPS.map(
        ({ src, tileSize }) =>
          new Promise<TileMap>((resolve) => {
            const image = new Image();
            image.src = src;
            image.addEventListener("load", () =>
              resolve(new TileMap(image, tileSize))
            );
          })
      )
    );
  }

  /**
   * Populate the grid such that it fills the canvas.
   *
   * @param width The width of the canvas.
   * @param height The height of the canvas.
   */
  populate(width: number, height: number) {
    const rRange = this.getTileRange(height);
    const cRange = this.getTileRange(width);

    for (let r = -1; r <= this.simulationSize; r++) {
      for (let c = -1; c <= this.simulationSize; c++) {
        if (
          r === -1 ||
          r === this.simulationSize ||
          c === -1 ||
          c === this.simulationSize
        ) {
          this.grid.set(r + "," + c, TileType.Fence);
        } else {
          this.grid.set(r + "," + c, TileType.Dirt);
        }
      }
    }

    this.grid.set("-2,-1", TileType.Grass);
    for (let r = 0; r < 2; r++) {
      for (let c = 0; c < 4; c++) {
        this.grid.set(
          this.simulationSize + 1 + r + "," + (-1 + c),
          TileType.Grass
        );
      }
    }

    for (let r = rRange[0]; r < rRange[1]; r++) {
      for (let c = cRange[0]; c < cRange[1]; c++) {
        if (this.grid.has(r + "," + c)) {
          continue;
        }

        const n = Math.random();

        if (n <= 0.01) {
          this.grid.set(r + "," + c, TileType.Mushrooms);
        } else if (n <= 0.7) {
          this.grid.set(r + "," + c, TileType.Grass);
        } else if (n <= 0.8) {
          this.grid.set(r + "," + c, TileType.Leaves);
        } else if (n <= 0.9) {
          this.grid.set(r + "," + c, TileType.Flowers);
        } else if (n <= 0.95) {
          this.grid.set(r + "," + c, TileType.Stalks);
        } else {
          this.grid.set(r + "," + c, TileType.Tree);
        }
      }
    }
  }

  /**
   * Get the range of currently visible tiles, for a single dimension.
   *
   * @param pixels The size of the dimension, in pixels.
   */
  getTileRange(pixels: number) {
    const tiles = this.getCameraPosition(pixels) / Grid.TILE_SIZE;
    return [0 - Math.ceil(tiles), this.simulationSize + Math.ceil(tiles)];
  }

  /**
   * Get the camera position such that the simulation area is centred, for a
   * single dimension.
   *
   * @param pixels The size of the dimension, in pixels.
   */
  getCameraPosition(pixels: number) {
    return Math.ceil((pixels - this.simulationSize * Grid.TILE_SIZE) / 2);
  }

  /**
   * Recalculate the grid tile size, based on the current DPR.
   */
  static recalculateTileSize() {
    if (window.devicePixelRatio <= 1) {
      this.TILE_SIZE = 64;
    } else if (window.devicePixelRatio <= 2) {
      this.TILE_SIZE = 48;
    } else {
      this.TILE_SIZE = 32;
    }
  }

  /**
   * Render the grid.
   *
   * @param ctx The 2D context to render to.
   */
  render(ctx: CanvasRenderingContext2D) {
    ctx.translate(
      this.getCameraPosition(ctx.canvas.width),
      this.getCameraPosition(ctx.canvas.height)
    );

    const rRange = this.getTileRange(ctx.canvas.height);
    const cRange = this.getTileRange(ctx.canvas.width);

    for (let r = rRange[0]; r < rRange[1]; r++) {
      for (let c = cRange[0]; c < cRange[1]; c++) {
        this.renderTile(ctx, r, c);
      }
    }

    ctx.resetTransform();
  }

  /**
   * Render a tile to the screen.
   *
   * @param ctx The 2D context to render to.
   * @param r The tile's row.
   * @param c The tile's column.
   */
  renderTile(ctx: CanvasRenderingContext2D, r: number, c: number) {
    const tileType = this.grid.get(r + "," + c);
    const tiles: [number, number][] = [];

    switch (tileType) {
      case TileType.Grass:
        tiles.push([0, 0]);
        break;

      case TileType.Leaves:
        tiles.push([0, 1]);
        break;

      case TileType.Flowers:
        tiles.push([0, 2]);
        break;

      case TileType.Tree:
        tiles.push([0, 0]);
        tiles.push([2, 4]);
        break;

      case TileType.Stalks:
        tiles.push([0, 0]);
        tiles.push([1, 5]);
        break;

      case TileType.Mushrooms:
        tiles.push([0, 0]);
        tiles.push([2, 5]);
        break;

      case TileType.Dirt: {
        const lookup: {
          [key: number]: [number, number];
        } = {
          3: [1, 2],
          6: [1, 0],
          7: [1, 1],
          9: [3, 2],
          11: [2, 2],
          12: [3, 0],
          13: [3, 1],
          14: [2, 0],
          15: [2, 1],
        };

        tiles.push(lookup[this.getIdenticalNeighbours(r, c)]);
        break;
      }

      case TileType.Fence: {
        tiles.push([0, 0]);

        const lookup: {
          [key: number]: [number, number];
        } = {
          3: [3, 10],
          5: [3, 9],
          6: [3, 8],
          9: [5, 10],
          10:
            this.grid.get(r + "," + (c + 1)) === TileType.Dirt
              ? [4, 8]
              : [4, 10],
          12: [5, 8],
        };

        tiles.push(lookup[this.getIdenticalNeighbours(r, c)]);
        break;
      }
    }

    for (const tile of tiles) {
      ctx.drawImage(
        this.tileMaps[1].image,
        ...this.tileMaps[1].getTile(...tile),
        c * Grid.TILE_SIZE,
        r * Grid.TILE_SIZE,
        Grid.TILE_SIZE,
        Grid.TILE_SIZE
      );
    }
  }

  /**
   * Get a bitmask of a tile's neighbours.
   *
   * The bits are ordered clockwise (i.e., URDL), and a one represents a
   * neighbour of the same type.
   *
   * @param r The tile's row.
   * @param c The tile's column.
   */
  getIdenticalNeighbours(r: number, c: number): number {
    const tileType = this.grid.get(r + "," + c);

    let mask = 0;

    if (this.grid.get(r - 1 + "," + c) === tileType) mask |= 1 << 3;
    if (this.grid.get(r + "," + (c + 1)) === tileType) mask |= 1 << 2;
    if (this.grid.get(r + 1 + "," + c) === tileType) mask |= 1 << 1;
    if (this.grid.get(r + "," + (c - 1)) === tileType) mask |= 1 << 0;

    return mask;
  }

  /**
   * Render entities to a grid cell.
   *
   * @param ctx The 2D context to render to.
   * @param r The tile's row.
   * @param c The tile's column.
   * @param entities The entities to render.
   */
  renderEntities(
    ctx: CanvasRenderingContext2D,
    r: number,
    c: number,
    entities: Entity[]
  ) {
    ctx.translate(
      this.getCameraPosition(ctx.canvas.width),
      this.getCameraPosition(ctx.canvas.height)
    );

    const tileSize = Math.floor(Grid.TILE_SIZE / entities.length);

    for (let i = 0; i < entities.length; i++) {
      const entity = entities[i];
      let tile: [number, number];

      switch (entity.type) {
        case EntityType.Ghost:
          tile = [10, 1];
          break;

        case EntityType.Human:
          tile = [7, 1];
          break;

        case EntityType.Gravestone:
          tile = [5, 4];
          break;
      }

      ctx.drawImage(
        this.tileMaps[0].image,
        ...this.tileMaps[0].getTile(...tile),
        c * Grid.TILE_SIZE + i * tileSize,
        r * Grid.TILE_SIZE + (entities.length - 1) * 0.5 * tileSize,
        tileSize,
        tileSize
      );

      if (entity.type === EntityType.Ghost) {
        ctx.drawImage(
          this.tileMaps[2].image,
          ...this.tileMaps[2].getTile(9, 17 + entity.id),
          c * Grid.TILE_SIZE + (i + 0.5) * tileSize,
          r * Grid.TILE_SIZE + (entities.length - 1) * 0.5 * tileSize,
          tileSize / 2,
          tileSize / 2
        );
      }
    }

    ctx.resetTransform();
  }

  /**
   * Render an interface element.
   *
   * @param ctx The 2D context to render to.
   * @param r The element's row.
   * @param c The element's column.
   * @param scale The scale of the element relative to the tile size.
   */
  renderInterfaceElement(
    ctx: CanvasRenderingContext2D,
    r: number,
    c: number,
    element: [number, number]
  ) {
    ctx.translate(
      this.getCameraPosition(ctx.canvas.width),
      this.getCameraPosition(ctx.canvas.height)
    );

    ctx.drawImage(
      this.tileMaps[2].image,
      ...this.tileMaps[2].getTile(...element),
      c * Grid.TILE_SIZE,
      r * Grid.TILE_SIZE,
      Grid.TILE_SIZE,
      Grid.TILE_SIZE
    );

    ctx.resetTransform();
  }
}

export default Grid;
