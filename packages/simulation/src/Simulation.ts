import Grid, { Entity, EntityType } from "./Grid";

/**
 * Represents a simulation state type.
 */
export enum StateType {
  Placing,
  Reading,
  Running,
}

/**
 * Represents a simulation state.
 */
export type State =
  | { type: StateType.Placing; placed: number }
  | { type: StateType.Reading }
  | { type: StateType.Running };

/**
 * Represents a move.
 */
export enum Move {
  Up,
  Right,
  Down,
  Left,
}

/**
 * Represents a simple ghost simulation.
 */
class Simulation {
  loaded: boolean;

  state: State;
  simulationSize: number;

  grid: Grid;
  entities: Entity[][][];

  hoveredTile: [number, number];
  moves: Move[];

  ghostCount: number;

  constructor() {
    this.loaded = false;

    this.state = { type: StateType.Placing, placed: 0 };
    this.simulationSize = 8;

    this.grid = new Grid(this.simulationSize);
    this.entities = new Array(this.simulationSize)
      .fill(null)
      .map(() => new Array(this.simulationSize).fill(null).map(() => []));

    this.hoveredTile = [-1, -1];
    this.moves = [];

    this.ghostCount = 1;
  }

  /**
   * Initialise the simulation and start the main loop.
   */
  async init() {
    await this.grid.init();

    this.loaded = true;
  }

  /**
   * Step the simulation forward.
   */
  step() {
    const modulus = (n: number, m: number) => ((n % m) + m) % m;

    for (let r = 0; r < this.simulationSize; r++) {
      for (let c = 0; c < this.simulationSize; c++) {
        const entities = this.entities[r][c];

        for (let e = entities.length - 1; e >= 0; e--) {
          const entity = entities[e];

          if (entity.type === EntityType.Ghost) {
            if (entity.hasMoved) continue;

            if (entity.steps !== this.moves.length) {
              entities.splice(e, 1);

              switch (this.moves[entity.steps]) {
                case Move.Up: {
                  const newR = modulus(r - 1, this.simulationSize);
                  this.entities[newR][c].push(entity);
                  break;
                }

                case Move.Right: {
                  const newC = modulus(c + 1, this.simulationSize);
                  this.entities[r][newC].push(entity);
                  break;
                }

                case Move.Down: {
                  const newR = modulus(r + 1, this.simulationSize);
                  this.entities[newR][c].push(entity);
                  break;
                }

                case Move.Left: {
                  const newC = modulus(c - 1, this.simulationSize);
                  this.entities[r][newC].push(entity);
                  break;
                }
              }

              entity.steps++;
              entity.hasMoved = true;
            }
          }
        }
      }
    }

    for (let r = 0; r < this.simulationSize; r++) {
      for (let c = 0; c < this.simulationSize; c++) {
        const entities = this.entities[r][c];

        for (let e = entities.length - 1; e >= 0; e--) {
          const entity = entities[e];

          if (entity.type === EntityType.Human) {
            for (let f = entities.length - 1; f >= 0; f--) {
              if (entities[f].type === EntityType.Ghost) {
                entities[e] = {
                  type: EntityType.Ghost,
                  id: this.ghostCount++,
                  steps: 0,
                  hasMoved: true,
                };
                entities.push({ type: EntityType.Gravestone });
                break;
              }
            }
          }
        }
      }
    }

    for (let r = 0; r < this.simulationSize; r++) {
      for (let c = 0; c < this.simulationSize; c++) {
        const entities = this.entities[r][c];

        for (let e = entities.length - 1; e >= 0; e--) {
          const entity = entities[e];

          if (entity.type === EntityType.Ghost) entity.hasMoved = false;
        }
      }
    }
  }

  /**
   * Render the simulation.
   *
   * This function will automatically queue itself to be called at the next
   * animation frame.
   */
  render(ctx: CanvasRenderingContext2D) {
    if (!this.loaded) return;

    this.grid.render(ctx);

    for (let r = 0; r < this.simulationSize; r++) {
      for (let c = 0; c < this.simulationSize; c++) {
        this.grid.renderEntities(ctx, r, c, this.entities[r][c]);
      }
    }

    if (this.state.type === StateType.Placing) {
      if (this.state.placed < 9) {
        const r = this.hoveredTile[0];
        const c = this.hoveredTile[1];

        if (this.inSimulationArea(r, c)) {
          if (this.entities[r][c].length === 0) {
            ctx.globalAlpha = 0.5;

            this.grid.renderEntities(ctx, r, c, [
              this.state.placed === 0
                ? { type: EntityType.Ghost, id: 0, steps: 0, hasMoved: false }
                : { type: EntityType.Human },
            ]);

            ctx.globalAlpha = 1;
          }
        }
      }
    }

    const stateLookup = new Map<StateType, [number, number]>([
      [StateType.Placing, [9, 28]],
      [StateType.Reading, [12, 18]],
      [StateType.Running, [15, 27]],
    ]);
    this.grid.renderInterfaceElement(
      ctx,
      -2,
      -1,
      stateLookup.get(this.state.type)!
    );

    for (let i = 0; i < this.moves.length; i++) {
      this.grid.renderInterfaceElement(
        ctx,
        this.simulationSize + 1 + Math.floor(i / 4),
        -1 + (i % 4),
        [12, 30 + this.moves[i]]
      );
    }
  }

  /**
   * Resize the grid to fill the screen.
   */
  resize(ctx: CanvasRenderingContext2D) {
    Grid.recalculateTileSize();
    this.grid.populate(ctx.canvas.width, ctx.canvas.height);
  }

  /**
   * Handler for 'mousemove' events.
   */
  onMouseMove(ev: React.MouseEvent<HTMLCanvasElement, MouseEvent>) {
    const pixelToTile = (pixel: number, pixels: number) => {
      const position = this.grid.getCameraPosition(pixels);
      return Math.floor((pixel - position) / Grid.TILE_SIZE);
    };

    this.hoveredTile = [
      pixelToTile(ev.clientY, ev.currentTarget.height),
      pixelToTile(ev.clientX, ev.currentTarget.width),
    ];
  }

  /**
   * Handler for 'click' events.
   */
  onClick() {
    if (this.state.type === StateType.Placing) {
      if (this.state.placed >= 9) return;

      const r = this.hoveredTile[0];
      const c = this.hoveredTile[1];

      if (this.inSimulationArea(r, c)) {
        if (this.entities[r][c].length === 0) {
          if (this.state.placed === 0) {
            this.entities[r][c] = [
              { type: EntityType.Ghost, id: 0, steps: 0, hasMoved: false },
            ];
          } else {
            this.entities[r][c] = [{ type: EntityType.Human }];
          }

          this.state.placed++;
        }
      }
    }
  }

  /**
   * Handler for 'keydown' events.
   */
  onKeyDown(ev: React.KeyboardEvent<HTMLCanvasElement>) {
    switch (this.state.type) {
      case StateType.Placing:
        if (ev.code === "Space") {
          if (this.state.placed >= 1) {
            this.state = { type: StateType.Reading };
          }
        }
        break;

      case StateType.Reading:
        switch (ev.code) {
          case "Space":
            if (this.moves.length > 2) {
              this.state = { type: StateType.Running };
            }

            break;

          case "Backspace":
          case "Delete":
            this.moves.pop();
            break;

          default: {
            // prettier-ignore
            const lookup: {
            [key: string]: Move;
          } = {
            "ArrowUp": Move.Up,
            "ArrowRight": Move.Right,
            "ArrowDown": Move.Down,
            "ArrowLeft": Move.Left,
          };
            const move = lookup[ev.code];

            if (move !== undefined) {
              if (this.moves.length < 8) {
                this.moves.push(move);
              }
            }
          }
        }
        break;

      case StateType.Running:
        if (ev.code === "Space") this.step();
        break;
    }
  }

  /**
   * Check whether a tile is within the simulation area.
   *
   * @param r The tile's row.
   * @param c The tile's column.
   * @returns True if the tile is within the simulation area.
   */
  inSimulationArea(r: number, c: number): boolean {
    return (
      r >= 0 && r < this.simulationSize && c >= 0 && c < this.simulationSize
    );
  }
}

export default Simulation;
