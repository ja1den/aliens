/**
 * Represents a grid of sprites, accessible by index.
 */
class TileMap {
  image: HTMLImageElement;
  tileSize: number;

  rows: number;
  cols: number;

  constructor(image: HTMLImageElement, tileSize: number) {
    this.image = image;
    this.tileSize = tileSize;

    this.rows = this.image.width / this.tileSize;
    this.cols = this.image.height / this.tileSize;
  }

  /**
   * Get the position and size of the sub-rectangle corresponding to a tile.
   *
   * @param r The tile's row.
   * @param c The tile's column.
   * @returns The sub-rectangle corresponding to the tile.
   */
  getTile(r: number, c: number): [number, number, number, number] {
    return [c * this.tileSize, r * this.tileSize, this.tileSize, this.tileSize];
  }
}

export default TileMap;
