Inheritance:

Remove Simulation class and move ALL render logic into index (or a submodule, but not a class).

<!-- prettier-ignore -->
Entity -> Alien
       -> Human
       -> Gravestone

Update loop:

1. Iterate through entities and step them.
2. Iterate through entities and collide them.
3. Apply creations and deletions.
