import { useEffect, useRef } from "react";

import Canvas from "../Canvas";

import Simulation from "../../lib/Simulation";
import SpriteManager, { SpriteSheet } from "../../lib/SpriteManager";
import Surface, { Tile } from "../../lib/Surface";

import classes from "./index.module.css";

const App = () => {
  const sizeRef = useRef(0);

  const simulationRef = useRef<Simulation | null>(null);
  const surfaceRef = useRef<Surface | null>(null);

  const spriteManagerRef = useRef<SpriteManager | null>(null);

  useEffect(() => {
    const url = new URL(window.location.href);
    let size = parseInt(url.searchParams.get("size") ?? "");

    if (isNaN(size)) size = 8;
    size = Math.min(Math.max(size, 2), 8);

    sizeRef.current = size;

    url.searchParams.set("size", size.toString());
    history.replaceState(null, "", url);
  }, []);

  useEffect(() => {
    simulationRef.current = new Simulation();
    surfaceRef.current = new Surface();
    surfaceRef.current.populate([-10, 10], [-10, 10]);

    (async () => {
      const SPRITE_SHEETS: SpriteSheet[] = [
        { src: "/img/01.png", spriteSize: 16 },
        { src: "/img/02.png", spriteSize: 16 },
        { src: "/img/03.png", spriteSize: 16 },
      ];

      const spriteManager = new SpriteManager(SPRITE_SHEETS);
      await spriteManager.load();
      spriteManagerRef.current = spriteManager;
    })();
  }, []);

  const draw = (ctx: CanvasRenderingContext2D) => {
    const surface = surfaceRef.current;
    const spriteManager = spriteManagerRef.current;

    if (surface === null) return;
    if (spriteManager === null) return;

    for (const [key, tile] of surface.grid.entries()) {
      const [x, y] = key.split(",").map((coordinate) => parseInt(coordinate));
    }
  };

  const handleMouseMove: React.MouseEventHandler<HTMLCanvasElement> = (ev) => {
    simulationRef.current?.onMouseMove(ev);
  };

  const handleClick: React.MouseEventHandler<HTMLCanvasElement> = () => {
    simulationRef.current?.onClick();
  };

  const handleKeyDown: React.KeyboardEventHandler<HTMLCanvasElement> = (ev) => {
    simulationRef.current?.onKeyDown(ev);
  };

  const handleResize = (ctx: CanvasRenderingContext2D) => {
    simulationRef.current?.resize(ctx);
  };

  return (
    <Canvas
      className={classes.canvas}
      draw={draw}
      onResize={handleResize}
      onMouseMove={handleMouseMove}
      onClick={handleClick}
      tabIndex={-1}
      onKeyDown={handleKeyDown}
    />
  );
};

export default App;
