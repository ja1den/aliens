import { Tile } from "../../../lib/Surface";
import SpriteManager, { Sprite } from "../../../lib/SpriteManager";

import type { Position } from "../../../lib/types";

/*
const surfaceSpriteLookup = new Map<
  Tile,
  Sprite, sp
>([
  [Tile.Grass, [1, 0, 0]],
  [Tile.Leaves, [1, 1, 0]],
]);
*/

/**
 * Render a tile to a context.
 *
 * @param ctx The context to render to.
 * @param position The position to render at.
 * @param tile The tile to render.
 */
const renderTile = (
  ctx: CanvasRenderingContext2D,
  position: Position,
  tile: Tile
) => {
  const tiles: [number, number][] = [];

  switch (tile) {
    case Tile.Grass:
      tiles.push([0, 0]);
      break;

    case Tile.Leaves:
      tiles.push([1, 0]);
      break;

    case Tile.Flowers:
      tiles.push([2, 0]);
      break;

    case Tile.Tree:
      tiles.push([0, 0]);
      tiles.push([4, 2]);
      break;

    case Tile.Stalks:
      tiles.push([0, 0]);
      tiles.push([5, 1]);
      break;

    case Tile.Mushrooms:
      tiles.push([0, 0]);
      tiles.push([5, 2]);
      break;

    case Tile.Dirt: {
      tiles.push([1, 2]);

      /*
          const lookup: {
            [key: number]: [number, number];
          } = {
            3: [1, 2],
            6: [1, 0],
            7: [1, 1],
            9: [3, 2],
            11: [2, 2],
            12: [3, 0],
            13: [3, 1],
            14: [2, 0],
            15: [2, 1],
          };

          tiles.push(lookup[this.getIdenticalNeighbours(r, c)]);
          */
      break;
    }

    case Tile.Fence: {
      tiles.push([0, 0]);

      /*
          const lookup: {
            [key: number]: [number, number];
          } = {
            3: [3, 10],
            5: [3, 9],
            6: [3, 8],
            9: [5, 10],
            10:
              this.grid.get(r + "," + (c + 1)) === Tile.Dirt ? [4, 8] : [4, 10],
            12: [5, 8],
          };

          tiles.push(lookup[this.getIdenticalNeighbours(r, c)]);
          */
      break;
    }
  }

  /*
  for (const tile of tiles) {
    ctx.drawImage(
      ...spriteManager.getSprite(1, ...tile),
      position[0] * 64,
      position[1] * 64,
      64,
      64
    );
  }
  */
};

export default renderTile;
