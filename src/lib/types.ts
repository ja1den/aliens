/**
 * Represents a position.
 */
export type Position = [number, number];
