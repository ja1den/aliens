import type SpriteManager from "./SpriteManager";

class SimulationRenderer {
  readonly spriteManager: SpriteManager;

  constructor(spriteManager: SpriteManager) {
    this.spriteManager = spriteManager;
  }
}

export default SimulationRenderer;
