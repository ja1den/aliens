import Entity, { Move } from "../Entity";

/**
 * Represents an alien.
 */
class Alien extends Entity {
  moves: Move[];
  steps: number;

  constructor(x: number, y: number, moves: Move[]) {
    super(x, y);

    this.moves = moves;
    this.steps = 0;
  }

  override step() {
    switch (this.moves[this.steps]) {
      case Move.Up:
        this.y--;
        break;

      case Move.Right:
        this.x++;
        break;

      case Move.Down:
        this.y++;
        break;

      case Move.Left:
        this.x--;
        break;
    }

    this.steps++;
  }

  override collide(other: Entity) {}
}

export default Alien;
