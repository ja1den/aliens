/**
 * Represents a direction.
 */
export enum Move {
  Up,
  Right,
  Down,
  Left,
}

/**
 * Represents an entity.
 */
class Entity {
  x: number;
  y: number;

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }

  /**
   * Step the entity forward.
   */
  step() {
    // no-op
  }

  /**
   * Handle a collision with another entity.
   *
   * @param other The other entity.
   */
  collide(_other: Entity) {
    // no-op
  }
}

export default Entity;
