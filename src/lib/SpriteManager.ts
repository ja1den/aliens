/**
 * Represents a sprite sheet.
 */
export type SpriteSheet = {
  src: string;
  spriteSize: number;
};

/**
 * Represents a sprite.
 */
export type Sprite = [HTMLImageElement, number, number, number, number];

/**
 * Stores and manages a collection of sprite sheets.
 */
class SpriteManager {
  sheets: SpriteSheet[];
  images: HTMLImageElement[];

  constructor(sheets: SpriteSheet[]) {
    this.sheets = sheets;
    this.images = sheets.map((sheet) => {
      const image = new Image();
      image.src = sheet.src;
      return image;
    });
  }

  /**
   * Load the provided sprite sheets.
   *
   * @returns A promise that resolves when all sheets have loaded.
   */
  async load() {
    return await Promise.all(
      this.images.map(
        (image) =>
          new Promise<void>((resolve) => {
            if (image.complete) {
              resolve();
            } else {
              image.addEventListener("load", () => resolve());
            }
          })
      )
    );
  }

  /**
   * Get a sprite from a sprite sheet.
   *
   * @param sheetID The ID of the sprite sheet to read from.
   * @param x The sprite's x-index.
   * @param y The sprite's y-index.
   */
  getSprite(sheetID: number, x: number, y: number): Sprite {
    const { spriteSize } = this.sheets[sheetID];
    const image = this.images[sheetID];
    return [image, x * spriteSize, y * spriteSize, spriteSize, spriteSize];
  }
}

export default SpriteManager;
