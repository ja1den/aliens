/**
 * Represents a range of numbers.
 */
export type Range = [number, number];

/**
 * Represents a tile.
 */
export enum Tile {
  Grass,
  Leaves,
  Flowers,
  Stalks,
  Tree,
  Mushrooms,
  Dirt,
  Fence,
}

/**
 * Represents a surface, consisting of a grid of {@linkcode Tile}s.
 */
class Surface {
  grid: Map<string, Tile> = new Map();

  /**
   * Construct a new Surface object.
   *
   * @param preset Provide a default layout that takes priority over population.
   */
  constructor(preset?: Map<string, Tile>) {
    if (preset) this.grid = preset;
  }

  /**
   * Populate the grid such that it is at least a minimum size.
   *
   * @param xRange The minimum range along the x-axis.
   * @param yRange The minimum range along the y-axis.
   */
  populate(xRange: Range, yRange: Range) {
    /*
    for (let r = -1; r <= this.size; r++) {
      for (let c = -1; c <= this.size; c++) {
        if (r === -1 || r === this.size || c === -1 || c === this.size) {
          this.grid.set(r + "," + c, MapTile.Fence);
        } else {
          this.grid.set(r + "," + c, MapTile.Dirt);
        }
      }
    }

    this.grid.set("-2,-1", MapTile.Grass);
    for (let r = 0; r < 2; r++) {
      for (let c = 0; c < 4; c++) {
        this.grid.set(this.size + 1 + r + "," + (-1 + c), MapTile.Grass);
      }
    }
    */

    for (let x = xRange[0]; x < xRange[1]; x++) {
      for (let y = yRange[0]; y < yRange[1]; y++) {
        if (this.grid.has(`${x},${y}`)) {
          continue;
        }

        const n = Math.random();

        if (n <= 0.01) {
          this.grid.set(`${x},${y}`, Tile.Mushrooms);
        } else if (n <= 0.7) {
          this.grid.set(`${x},${y}`, Tile.Grass);
        } else if (n <= 0.8) {
          this.grid.set(`${x},${y}`, Tile.Leaves);
        } else if (n <= 0.9) {
          this.grid.set(`${x},${y}`, Tile.Flowers);
        } else if (n <= 0.95) {
          this.grid.set(`${x},${y}`, Tile.Stalks);
        } else {
          this.grid.set(`${x},${y}`, Tile.Tree);
        }
      }
    }
  }

  /**
   * Get a bitmask of a tile's neighbours.
   *
   * The bits are ordered clockwise (i.e., URDL), and a set bit represents a
   * neighbour of the same type.
   *
   * @param x The tile's x-coordinate.
   * @param y The tile's y-coordinate.
   */
  getIdenticalNeighbours(x: number, y: number): number {
    const tileType = this.grid.get(`${x},${y}`);

    let mask = 0;

    if (this.grid.get(`${x},${y - 1}`) === tileType) mask |= 1 << 3;
    if (this.grid.get(`${x + 1},${y}`) === tileType) mask |= 1 << 2;
    if (this.grid.get(`${x},${y + 1}`) === tileType) mask |= 1 << 1;
    if (this.grid.get(`${x - 1},${y}`) === tileType) mask |= 1 << 0;

    return mask;
  }
}

export default Surface;
