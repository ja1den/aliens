# Ghosts

> Simple ghost simulation.

## Introduction

This repository contains the source code for a simple ghost simulation.

## Usage

The simulation has three stages, each indicated by a symbol at the top left of the grid.\
To move to the next stage, press <kbd>Space</kbd>.

The stages are as follows:

1. Placement, indicated by a plus symbol.\
   In this stage, you place a ghost and up to eight humans on the grid.
2. Movement, indicated by a grid symbol.\
   In this stage, you provide a list of between two and eight moves to be followed by each ghost. Moves are added to the list by pressing the arrow keys. The most recent move can be deleted by pressing <kbd>Backspace</kbd>.
3. Execution, indicated by a play symbol.\
   In this stage, you step through the simulation by pressing <kbd>Space</kbd>.

To change the size of the simulation area, pass a `size` between `2` and `8` (inclusive) in the URL.

## Contributing

Clone the repository:

```sh
git clone git@gitlab.com:ja1den/ghosts.git
```

Install the dependencies with [`npm`](https://npmjs.com/):

```sh
npm install
```

### Development

Start the [Vite](https://vitejs.dev/) development server:

```sh
npm run dev
```

Then, open your browser to http://localhost:5173/.

Lint the project with [ESLint](https://eslint.org/) and [Prettier](https://prettier.io/).

```
npm run lint
```

### Production

Generate a production build of the website:

```sh
npm run build
```

Preview the production build locally:

```sh
npm run preview
```

## Credits

The assets for this project come from [Kenney](https://kenney.nl/).

The following packs are used:

- [Tiny Dungeon](https://kenney.nl/assets/tiny-dungeon)
- [Tiny Town](https://kenney.nl/assets/tiny-town)
- [1-Bit Input Prompts Pixel 16×](https://kenney.nl/assets/bit-input-prompts-pixel-16)
