import { useEffect, useRef } from "react";

import Canvas from "./Canvas";

import Simulation from "../lib/Simulation";

import classes from "./App.module.css";

const App = () => {
  const simulationRef = useRef<Simulation | null>(null);

  useEffect(() => {
    simulationRef.current = new Simulation();
    simulationRef.current?.init();
  }, []);

  const draw = (ctx: CanvasRenderingContext2D) => {
    if (simulationRef.current?.grid.grid.size === 0)
      simulationRef.current?.resize(ctx);

    simulationRef.current?.render(ctx);
  };

  const handleMouseMove: React.MouseEventHandler<HTMLCanvasElement> = (ev) => {
    simulationRef.current?.onMouseMove(ev);
  };

  const handleClick: React.MouseEventHandler<HTMLCanvasElement> = () => {
    simulationRef.current?.onClick();
  };

  const handleKeyDown: React.KeyboardEventHandler<HTMLCanvasElement> = (ev) => {
    simulationRef.current?.onKeyDown(ev);
  };

  const handleResize = (ctx: CanvasRenderingContext2D) => {
    simulationRef.current?.resize(ctx);
  };

  return (
    <Canvas
      className={classes.canvas}
      draw={draw}
      onResize={handleResize}
      onMouseMove={handleMouseMove}
      onClick={handleClick}
      tabIndex={-1}
      onKeyDown={handleKeyDown}
    />
  );
};

export default App;
