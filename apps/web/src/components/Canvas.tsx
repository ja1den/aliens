import React, { useEffect, useRef } from "react";

import classes from "./Canvas.module.css";

export interface CanvasProps
  extends Omit<React.ComponentPropsWithoutRef<"canvas">, "onResize"> {
  draw: (ctx: CanvasRenderingContext2D) => void;
  onResize?: (ctx: CanvasRenderingContext2D) => void;
}

const Canvas = ({ className = "", draw, onResize, ...props }: CanvasProps) => {
  const canvasRef = useRef<HTMLCanvasElement>(null);

  useEffect(() => {
    const ctx = canvasRef.current?.getContext("2d");

    if (!ctx) return;

    ctx.imageSmoothingEnabled = false;

    const resize = () => {
      const width = ctx.canvas.clientWidth;
      const height = ctx.canvas.clientHeight;

      if (ctx.canvas.width === width && ctx.canvas.height === height) return;

      ctx.canvas.width = width;
      ctx.canvas.height = height;

      ctx.imageSmoothingEnabled = false;

      onResize?.(ctx);
    };

    const render = () => {
      resize();

      ctx.save();
      ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

      draw(ctx);

      ctx.restore();

      animationFrameId = requestAnimationFrame(render);
    };
    let animationFrameId = requestAnimationFrame(render);

    return () => {
      cancelAnimationFrame(animationFrameId);
    };
  }, [draw]);

  return (
    <canvas
      {...props}
      className={`${classes.canvas} ${className}`.trim()}
      ref={canvasRef}
    />
  );
};

export default Canvas;
